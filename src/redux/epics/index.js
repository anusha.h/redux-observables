import { ajax } from "rxjs/ajax";
import { switchMap, map } from "rxjs/operators";
import { ofType, combineEpics } from "redux-observable";
import { FETCH_REQUEST, fetchSuccess } from "../actions";

const fetchRequestEpic = (action$) => {
  return action$.pipe(
    ofType(FETCH_REQUEST),
    switchMap(({ payload }) => {
      return ajax({
        url: "https://test-api-fake-dummy.herokuapp.com/apply-coupon",
        method: "POST",
        body: { coupon: payload },
      }).pipe(
        map((data) => {
          return fetchSuccess(data.response);
        })
      );
    })
  );
};

export const rootEpic = combineEpics(fetchRequestEpic);
